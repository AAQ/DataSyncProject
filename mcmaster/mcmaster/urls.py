"""mcmaster URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from finalapp import views
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
#from rest_framework.schemas import get_schema_view
from rest_framework_swagger.views import get_swagger_view
# from rest_framework_jwt.views import obtain_jwt_token
from django.conf.urls.static import static
from django.conf import settings


schema_view = get_swagger_view(title='McQuest API')
#from finalapp.HybridRouter import HybridRouter

#router = HybridRouter()


router = routers.DefaultRouter()
#router.register(r'users', views.UserViewset)
#router.register(r'groups', views.GroupViewset)
router.register(r'sensordata', views.SensorDataViewset)
router.register(r'upload-data', views.UploadModelViewset)
router.register(r'historydb', views.HistorydbViewset)
router.register(r'install', views.InstallViewset)
#router.register(r'outline', views.OutlineViewset)
#router.register(r'buildlist', views.buildlistViewset)
#router.register(r'buildingLocation', views.buildingLocationViewset)
#router.register(r'firstfloor', views.Room1fViewset)
#router.register(r'2ndfloor', views.Room2fViewset)
#router.register(r'3rdfloor', views.Room3fViewset)
#router.register(r'4thfloor', views.Room4fViewset)
#router.register(r'5thfloor', views.Room5fViewset)
#router.register(r'6thfloor', views.Room6fViewset)
#router.register(r'7thfloor', views.Room7fViewset)
#router.register(r'firstbasement', views.Room101fViewset)
#router.register(r'secondbasement', views.Room102fViewset)
#router.register(r'pathway1', views.Pathway1fViewset)
#router.register(r'pathway2', views.Pathway2FViewset)
#router.register(r'pathway3', views.Pathway3FViewset)
#router.register(r'pathway4', views.Pathway4FViewset)
#router.register(r'pathway5', views.Pathway5FViewset)
#router.register(r'pathway6', views.Pathway6FViewset)
#router.register(r'pathway7', views.Pathway7FViewset)
#router.register(r'pathwaybasement1', views.Pathway101FViewset)
#router.register(r'pathwaybasement2', views.Pathway102FViewset)


urlpatterns = [
   url(r'^admin/', admin.site.urls),  
   url(r'^', include(router.urls)),
   #url(r'^api-token-auth/', obtain_jwt_token),
  # url(r'^api-token-verify/', verify_jwt_token),

#   url(r'^install/', views.InstallViewset.as_view()),
   url(r'^api-auth/', include('rest_framework.urls', namespace= 'rest_framework')),
   url(r'^schema/$', schema_view),
   url(r'^support/', include('support.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) 

urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)


