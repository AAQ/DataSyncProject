# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.contrib.gis.db import models


class Pathway101F(models.Model):
    pid = models.IntegerField(db_column='PID', blank=True, null=True)  # Field name made lowercase.
    nodefrom = models.IntegerField(db_column='NodeFrom', blank=True, null=True)  # Field name made lowercase.
    nodeto = models.IntegerField(db_column='NodeTo', blank=True, null=True)  # Field name made lowercase.
    cost = models.TextField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.

