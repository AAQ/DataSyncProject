# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
import json


def check_version(request):
    new_version_code = 1.1
    client_version = request.GET['clientVersion']
    response_data = {}

    # TODO 
    if new_version_code > float(client_version):
        response_data['changed'] = True
    else :
        response_data['changed'] = False
    response_data['link'] = "path-to-download"
    response_data['latestVersion'] = str(new_version_code)
    response_data['clientVersion'] = client_version

    return HttpResponse(json.dumps(response_data), content_type="application/json")

