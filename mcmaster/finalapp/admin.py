# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin 
from leaflet.admin import LeafletGeoAdmin 
from django.contrib.gis import admin
from models import *

# Register your models here.


class Pathway101FAdmin(LeafletGeoAdmin):
	search_fields=('pid', 'cost')

class Pathway102FAdmin(LeafletGeoAdmin):
	search_fields=('pid', 'cost')

class Pathway2FAdmin(LeafletGeoAdmin):
	search_fields=('pid', 'cost')

class Pathway3FAdmin(LeafletGeoAdmin):
	search_fields=('pid', 'cost')

class Pathway4FAdmin(LeafletGeoAdmin):
	search_fields=('pid', 'cost')

class Pathway5FAdmin(LeafletGeoAdmin):
	search_fields=('pid', 'cost')

class Pathway6FAdmin(LeafletGeoAdmin):
	search_fields=('pid', 'cost')

class Pathway7FAdmin(LeafletGeoAdmin):
	search_fields=('pid', 'cost')

class PathWay101f_net_dataAdmin(LeafletGeoAdmin):
	search_fields=('id', 'NetworkData')

class PathWay102f_net_dataAdmin(LeafletGeoAdmin):
	search_fields=('id', 'NetworkData')

class PathWay1f_net_dataAdmin(LeafletGeoAdmin):
	search_fields=('id', 'NetworkData')

class PathWay2f_net_dataAdmin(LeafletGeoAdmin):
	search_fields=('id', 'NetworkData')

class PathWay3f_net_dataAdmin(LeafletGeoAdmin):
	search_fields=('id', 'NetworkData')

class PathWay4f_net_dataAdmin(LeafletGeoAdmin):
	search_fields=('id', 'NetworkData')

class PathWay5f_net_dataAdmin(LeafletGeoAdmin):
	search_fields=('id', 'NetworkData')

class PathWay6f_net_dataAdmin(LeafletGeoAdmin):
	search_fields=('id', 'NetworkData')

class PathWay7f_net_dataAdmin(LeafletGeoAdmin):
	search_fields=('id', 'NetworkData')

class Room3fAdmin(LeafletGeoAdmin):
	search_fields=('name','floor', 'utility')

class Room4fAdmin(LeafletGeoAdmin):
	search_fields=('name','floor', 'utility')

class Room5fAdmin(LeafletGeoAdmin):
	search_fields=('name','floor', 'utility')

class Room2fAdmin(LeafletGeoAdmin):
	search_fields=('name','floor', 'utility')

class Room6fAdmin(LeafletGeoAdmin):
	search_fields=('name','floor', 'utility')

class Room7fAdmin(LeafletGeoAdmin):
	search_fields=('name','floor', 'utility')

class Room101fAdmin(LeafletGeoAdmin):
	search_fields=('name','floor', 'utility')

class Room102fAdmin(LeafletGeoAdmin):
	search_fields=('name','floor', 'utility')

class Room1fAdmin(LeafletGeoAdmin):
	search_fields=('name','floor', 'utility')

class OutlineAdmin(LeafletGeoAdmin):
	search_fields=('name','location', 'utility')

class buildingLocationAdmin(LeafletGeoAdmin):
	search_fields=('showname', 'location')

class buildlistAdmin(LeafletGeoAdmin):
	search_fields=('showname', 'location')

class SensorDataAdmin(LeafletGeoAdmin):
	search_fields=('building_id','floor_level')

class InstallDataAdmin(LeafletGeoAdmin):
	search_fields=('uuid','timeinstalled')

class UploadModelAdmin(LeafletGeoAdmin):
	search_fields=('title', 'file')

class HistorydbAdmin(LeafletGeoAdmin):
	search_fields=('uuid', 'name')

admin.site.register(UploadModel,  UploadModelAdmin)
admin.site.register(Historydb,  HistorydbAdmin)
admin.site.register(Install, InstallDataAdmin)
admin.site.register(SensorData, SensorDataAdmin)
admin.site.register(buildlist, buildlistAdmin)
admin.site.register(buildingLocation, buildingLocationAdmin)
admin.site.register(Outline, OutlineAdmin)
admin.site.register(Room3f, Room3fAdmin)
admin.site.register(Room2f, Room2fAdmin)
admin.site.register(Room1f, Room1fAdmin)
admin.site.register(Room4f, Room4fAdmin)
admin.site.register(Room5f, Room5fAdmin)
admin.site.register(Room6f, Room6fAdmin)
admin.site.register(Room7f, Room7fAdmin)
admin.site.register(Room101f, Room101fAdmin)
admin.site.register(Room102f, Room102fAdmin)
admin.site.register(Pathway101F, Pathway101FAdmin)
admin.site.register(Pathway102F, Pathway102FAdmin)
admin.site.register(Pathway2F, Pathway2FAdmin)
admin.site.register(Pathway3F, Pathway3FAdmin)
admin.site.register(Pathway4F, Pathway4FAdmin)
admin.site.register(Pathway5F, Pathway5FAdmin)
admin.site.register(Pathway6F, Pathway6FAdmin)
admin.site.register(Pathway7F, Pathway7FAdmin)
admin.site.register( PathWay101f_net_data,  PathWay101f_net_dataAdmin)
admin.site.register( PathWay102f_net_data,  PathWay102f_net_dataAdmin)
admin.site.register( PathWay1f_net_data,  PathWay1f_net_dataAdmin)
admin.site.register( PathWay2f_net_data,  PathWay2f_net_dataAdmin)
admin.site.register( PathWay3f_net_data,  PathWay3f_net_dataAdmin)
admin.site.register( PathWay4f_net_data,  PathWay4f_net_dataAdmin)
admin.site.register( PathWay5f_net_data,  PathWay5f_net_dataAdmin)
admin.site.register( PathWay6f_net_data,  PathWay6f_net_dataAdmin)
admin.site.register( PathWay7f_net_data,  PathWay7f_net_dataAdmin)
