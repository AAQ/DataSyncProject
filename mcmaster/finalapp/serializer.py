from django.contrib.auth.models import User, Group
#from .models import User as Install
from rest_framework import serializers
from .models import  PathWay1f
from .models import  Pathway101F
from .models import  Pathway102F
from .models import  Pathway2F
from .models import  Pathway3F
from .models import  Pathway4F
from .models import  Pathway5F
from .models import  Pathway6F
from .models import  Pathway7F
from .models import  Room3f
from .models import  Room4f
from .models import  Room5f
from .models import  Room6f
from .models import  Room7f
from .models import  Room101f
from .models import  Room102f
from .models import  Room1f
from .models import  Room3f
from .models import  Room2f
from .models import  Outline
from .models import buildingLocation
from .models import buildlist
from .models import SensorData
from .models import Install
from .models import Historydb
from .models import UploadModel




class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class OutlineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Outline
	fields = ('outid', 'name', 'poly', 'shortname', 'utility', 'oldname','num_floor', 'num_basement', 'showname', 'location')

class buildlistSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = buildlist
	fields = ('outid', 'showname', 'st_astext')

class buildingLocationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = buildingLocation
	fields = ('outid', 'showname', 'location')

class Room2fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room2f
	fields = ('Rid','outid','name','utility','floor','location', 'Geometry')

class Pathway1fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PathWay1f
	fields = ('PID','NodeFrom','NodeTo','Cost','Geometry','Indoor_more_cost', 'Indoor_pathway')


class Room1fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room1f
	fields = ('Rid','outid','name','utility','floor','location', 'NearestStaircase', 'buildingname')


class Room3fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room3f
	fields = ('Rid','outid','name','utility','floor','location')

class Room4fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room4f
	fields = ('Rid','outid','name','utility','floor','location')

class Room5fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room5f
	fields = ('Rid','outid','name','utility','floor','location')

class Room6fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room6f
	fields = ('Rid','outid','name','utility','floor','location')

class Room7fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room7f
	fields = ('Rid','outid','name','utility','floor','location')

class Room101fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room101f
	fields = ('Rid','outid','name','utility','floor','location')

class Room102fSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room102f
	fields = ('Rid','outid','name','utility','floor','location')

class Pathway2FSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pathway2F
	fields = ('pid','nodefrom','nodeto','cost','geometry')

class Pathway3FSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pathway3F
	fields = ('pid','nodefrom','nodeto','cost','geometry')

class Pathway4FSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pathway4F
	fields = ('pid','nodefrom','nodeto','cost','geometry')

class Pathway5FSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pathway5F
	fields = ('pid','nodefrom','nodeto','cost','geometry')

class Pathway6FSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pathway6F
	fields = ('pid','nodefrom','nodeto','cost','geometry')

class Pathway7FSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pathway7F
	fields = ('pid','nodefrom','nodeto','cost','geometry')

class Pathway101FSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pathway101F
	fields = ('pid','nodefrom','nodeto','cost','geometry')

class Pathway102FSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pathway3F
	fields = ('pid','nodefrom','nodeto','cost','geometry')

class SensorDataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SensorData
	fields = ('building_id','entrance_id','floor_level','light_sensor','magnetic_sensor', 'wifi_data', 'pressure_sensor')

class InstallSerializer(serializers.ModelSerializer):
     class Meta:
        model = Install
        fields = ('uuid', 'timeinstalled')

class HistorydbSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Historydb
	fields = ('uuid','name','shortname','room','outid', 'location', 'utility')

class UploadModelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UploadModel
        fields = ('id','url','title', 'file', 'Upload_date')

