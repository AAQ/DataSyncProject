# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-18 03:45
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='buildingLocation',
            fields=[
                ('outid', models.IntegerField(primary_key=True, serialize=False)),
                ('showname', models.TextField(null=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
            ],
            options={
                'db_table': 'buildingLocation',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='buildlist',
            fields=[
                ('outid', models.IntegerField(primary_key=True, serialize=False)),
                ('showname', models.TextField(null=True)),
                ('st_astext', models.TextField(null=True)),
            ],
            options={
                'db_table': 'buildlist',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Outline',
            fields=[
                ('outid', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.TextField(blank=True, null=True)),
                ('poly', django.contrib.gis.db.models.fields.GeometryField(srid=0)),
                ('shortname', models.TextField(blank=True, null=True)),
                ('utility', models.TextField(blank=True, null=True)),
                ('oldname', models.TextField(blank=True, null=True)),
                ('num_floor', models.IntegerField(blank=True, null=True)),
                ('num_basement', models.IntegerField(blank=True, null=True)),
                ('showname', models.TextField(blank=True, null=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=0)),
            ],
            options={
                'db_table': 'outline',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pathway101F',
            fields=[
                ('pid', models.IntegerField(db_column='PID', primary_key=True, serialize=False)),
                ('nodefrom', models.IntegerField(blank=True, db_column='NodeFrom', null=True)),
                ('nodeto', models.IntegerField(blank=True, db_column='NodeTo', null=True)),
                ('cost', models.FloatField(blank=True, db_column='Cost', null=True)),
                ('geometry', django.contrib.gis.db.models.fields.LineStringField(db_column='geometry', srid=4326)),
            ],
            options={
                'db_table': 'Pathway101F',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay101f_net_data',
            fields=[
                ('Id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('NetworkData', models.BinaryField(db_column='NetworkData', null=True)),
            ],
            options={
                'db_table': 'PathWay101f_net_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pathway102F',
            fields=[
                ('pid', models.IntegerField(db_column='PID', primary_key=True, serialize=False)),
                ('nodefrom', models.IntegerField(blank=True, db_column='NodeFrom', null=True)),
                ('nodeto', models.IntegerField(blank=True, db_column='NodeTo', null=True)),
                ('cost', models.FloatField(blank=True, db_column='Cost', null=True)),
                ('geometry', django.contrib.gis.db.models.fields.LineStringField(db_column='geometry', srid=4326)),
            ],
            options={
                'db_table': 'Pathway102F',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay102f_net_data',
            fields=[
                ('Id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('NetworkData', models.BinaryField(db_column='NetworkData', null=True)),
            ],
            options={
                'db_table': 'PathWay102f_net_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay1f',
            fields=[
                ('PID', models.IntegerField(primary_key=True, serialize=False)),
                ('NodeFrom', models.IntegerField(db_column='NodeFrom')),
                ('NodeTo', models.IntegerField(db_column='NodeTo')),
                ('Cost', models.FloatField(blank=True, db_column='Cost', null=True)),
                ('Geometry', django.contrib.gis.db.models.fields.LineStringField(db_column='geometry', srid=4326)),
                ('Indoor_more_cost', models.FloatField(blank=True, null=True)),
                ('Indoor_pathway', models.BinaryField(null=True)),
            ],
            options={
                'db_table': 'PathWay1f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay1f_net_data',
            fields=[
                ('Id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('NetworkData', models.BinaryField(db_column='NetworkData', null=True)),
            ],
            options={
                'db_table': 'PathWay1f_net_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pathway2F',
            fields=[
                ('pid', models.IntegerField(db_column='PID', primary_key=True, serialize=False)),
                ('nodefrom', models.IntegerField(blank=True, db_column='NodeFrom', null=True)),
                ('nodeto', models.IntegerField(blank=True, db_column='NodeTo', null=True)),
                ('cost', models.FloatField(blank=True, db_column='Cost', null=True)),
                ('geometry', django.contrib.gis.db.models.fields.LineStringField(db_column='geometry', srid=4326)),
            ],
            options={
                'db_table': 'Pathway2F',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay2f_net_data',
            fields=[
                ('Id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('NetworkData', models.BinaryField(db_column='NetworkData', null=True)),
            ],
            options={
                'db_table': 'PathWay2f_net_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pathway3F',
            fields=[
                ('pid', models.IntegerField(db_column='PID', primary_key=True, serialize=False)),
                ('nodefrom', models.IntegerField(blank=True, db_column='NodeFrom', null=True)),
                ('nodeto', models.IntegerField(blank=True, db_column='NodeTo', null=True)),
                ('cost', models.FloatField(blank=True, db_column='Cost', null=True)),
                ('geometry', django.contrib.gis.db.models.fields.LineStringField(db_column='geometry', srid=4326)),
            ],
            options={
                'db_table': 'Pathway3F',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay3f_net_data',
            fields=[
                ('Id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('NetworkData', models.BinaryField(db_column='NetworkData', null=True)),
            ],
            options={
                'db_table': 'PathWay3f_net_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pathway4F',
            fields=[
                ('pid', models.IntegerField(db_column='PID', primary_key=True, serialize=False)),
                ('nodefrom', models.IntegerField(blank=True, db_column='NodeFrom', null=True)),
                ('nodeto', models.IntegerField(blank=True, db_column='NodeTo', null=True)),
                ('cost', models.FloatField(blank=True, db_column='Cost', null=True)),
                ('geometry', django.contrib.gis.db.models.fields.LineStringField(db_column='geometry', srid=4326)),
            ],
            options={
                'db_table': 'Pathway4F',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay4f_net_data',
            fields=[
                ('Id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('NetworkData', models.BinaryField(db_column='NetworkData', null=True)),
            ],
            options={
                'db_table': 'PathWay4f_net_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pathway5F',
            fields=[
                ('pid', models.IntegerField(db_column='PID', primary_key=True, serialize=False)),
                ('nodefrom', models.IntegerField(blank=True, db_column='NodeFrom', null=True)),
                ('nodeto', models.IntegerField(blank=True, db_column='NodeTo', null=True)),
                ('cost', models.FloatField(blank=True, db_column='Cost', null=True)),
                ('geometry', django.contrib.gis.db.models.fields.LineStringField(db_column='geometry', srid=4326)),
            ],
            options={
                'db_table': 'Pathway5F',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay5f_net_data',
            fields=[
                ('Id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('NetworkData', models.BinaryField(db_column='NetworkData', null=True)),
            ],
            options={
                'db_table': 'PathWay5f_net_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pathway6F',
            fields=[
                ('pid', models.IntegerField(db_column='PID', primary_key=True, serialize=False)),
                ('nodefrom', models.IntegerField(blank=True, db_column='NodeFrom', null=True)),
                ('nodeto', models.IntegerField(blank=True, db_column='NodeTo', null=True)),
                ('cost', models.FloatField(blank=True, db_column='Cost', null=True)),
                ('geometry', django.contrib.gis.db.models.fields.LineStringField(db_column='geometry', srid=4326)),
            ],
            options={
                'db_table': 'Pathway6F',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay6f_net_data',
            fields=[
                ('Id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('NetworkData', models.BinaryField(db_column='NetworkData', null=True)),
            ],
            options={
                'db_table': 'PathWay6f_net_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pathway7F',
            fields=[
                ('pid', models.IntegerField(db_column='PID', primary_key=True, serialize=False)),
                ('nodefrom', models.IntegerField(blank=True, db_column='NodeFrom', null=True)),
                ('nodeto', models.IntegerField(blank=True, db_column='NodeTo', null=True)),
                ('cost', models.FloatField(blank=True, db_column='Cost', null=True)),
                ('geometry', django.contrib.gis.db.models.fields.LineStringField(db_column='geometry', srid=4326)),
            ],
            options={
                'db_table': 'Pathway7F',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathWay7f_net_data',
            fields=[
                ('Id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('NetworkData', models.BinaryField(db_column='NetworkData', null=True)),
            ],
            options={
                'db_table': 'PathWay7f_net_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Room101f',
            fields=[
                ('Rid', models.IntegerField(db_column='rid', primary_key=True, serialize=False)),
                ('outid', models.IntegerField(db_column='outid')),
                ('name', models.TextField(db_column='name')),
                ('utility', models.TextField(db_column='utility')),
                ('floor', models.IntegerField(db_column='floor')),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
            ],
            options={
                'db_table': 'Room101f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Room102f',
            fields=[
                ('Rid', models.IntegerField(db_column='rid', primary_key=True, serialize=False)),
                ('outid', models.IntegerField(db_column='outid')),
                ('name', models.TextField(db_column='name')),
                ('utility', models.TextField(db_column='utility')),
                ('floor', models.IntegerField(db_column='floor')),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
            ],
            options={
                'db_table': 'Room102f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Room1f',
            fields=[
                ('Rid', models.IntegerField(db_column='rid', primary_key=True, serialize=False)),
                ('outid', models.IntegerField(db_column='outid')),
                ('name', models.TextField(db_column='name')),
                ('utility', models.TextField(db_column='utility')),
                ('floor', models.IntegerField(db_column='floor')),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
                ('NearestStaircase', models.TextField(db_column='NearestStaircase')),
                ('buildingname', models.TextField(db_column='buildingname')),
            ],
            options={
                'db_table': 'Room1f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Room2f',
            fields=[
                ('Rid', models.IntegerField(db_column='rid', primary_key=True, serialize=False)),
                ('outid', models.IntegerField(db_column='outid')),
                ('name', models.TextField(db_column='name')),
                ('utility', models.TextField(db_column='utility')),
                ('floor', models.IntegerField(db_column='floor')),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
                ('Geometry', django.contrib.gis.db.models.fields.GeometryField(srid=0)),
            ],
            options={
                'db_table': 'Room2f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Room3f',
            fields=[
                ('Rid', models.IntegerField(db_column='rid', primary_key=True, serialize=False)),
                ('outid', models.IntegerField(db_column='outid')),
                ('name', models.TextField(db_column='name')),
                ('utility', models.TextField(db_column='utility')),
                ('floor', models.IntegerField(db_column='floor')),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
            ],
            options={
                'db_table': 'Room3f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Room4f',
            fields=[
                ('Rid', models.IntegerField(db_column='rid', primary_key=True, serialize=False)),
                ('outid', models.IntegerField(db_column='outid')),
                ('name', models.TextField(db_column='name')),
                ('utility', models.TextField(db_column='utility')),
                ('floor', models.IntegerField(db_column='floor')),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
            ],
            options={
                'db_table': 'Room4f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Room5f',
            fields=[
                ('Rid', models.IntegerField(db_column='rid', primary_key=True, serialize=False)),
                ('outid', models.IntegerField(db_column='outid')),
                ('name', models.TextField(db_column='name')),
                ('utility', models.TextField(db_column='utility')),
                ('floor', models.IntegerField(db_column='floor')),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
            ],
            options={
                'db_table': 'Room5f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Room6f',
            fields=[
                ('Rid', models.IntegerField(db_column='rid', primary_key=True, serialize=False)),
                ('outid', models.IntegerField(db_column='outid')),
                ('name', models.TextField(db_column='name')),
                ('utility', models.TextField(db_column='utility')),
                ('floor', models.IntegerField(db_column='floor')),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
            ],
            options={
                'db_table': 'Room6f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Room7f',
            fields=[
                ('Rid', models.IntegerField(db_column='rid', primary_key=True, serialize=False)),
                ('outid', models.IntegerField(db_column='outid')),
                ('name', models.TextField(db_column='name')),
                ('utility', models.TextField(db_column='utility')),
                ('floor', models.IntegerField(db_column='floor')),
                ('location', django.contrib.gis.db.models.fields.PointField(db_column='location', srid=0)),
            ],
            options={
                'db_table': 'Room7f',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.TextField(blank=True, null=True)),
                ('timeinstalled', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'user',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='CrowdData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.TextField()),
                ('acc', models.TextField(blank=True, null=True)),
                ('uploaded_time', models.DateTimeField(blank=True, null=True)),
                ('gyro', models.TextField(blank=True, null=True)),
                ('mag', models.TextField(blank=True, null=True)),
                ('light', models.TextField()),
                ('wifi', models.TextField()),
                ('lat', models.FloatField()),
                ('lon', models.FloatField()),
                ('floor', models.IntegerField()),
            ],
        ),
    ]
