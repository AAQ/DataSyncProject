# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse 
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.models import User, Group
import datetime
import json
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import detail_route
from django.shortcuts import render
from serializer import UserSerializer
from serializer import HistorydbSerializer
from serializer import GroupSerializer
from serializer import InstallSerializer
from serializer import OutlineSerializer
from serializer import Pathway101FSerializer
from serializer import Pathway102FSerializer
from serializer import Pathway1fSerializer
from serializer import Pathway2FSerializer
from serializer import Pathway3FSerializer
from serializer import Pathway4FSerializer
from serializer import Pathway5FSerializer
from serializer import Pathway6FSerializer
from serializer import Pathway7FSerializer
from serializer import Room3fSerializer
from serializer import Room1fSerializer
from serializer import Room2fSerializer
from serializer import Room4fSerializer
from serializer import Room5fSerializer
from serializer import Room6fSerializer
from serializer import Room7fSerializer
from serializer import Room101fSerializer
from serializer import Room102fSerializer
from serializer import buildingLocationSerializer
from serializer import buildlistSerializer
from serializer import SensorDataSerializer
from serializer import UploadModelSerializer
from .models import Install
from rest_framework.parsers import FileUploadParser
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view
from rest_framework.views import APIView
#from filetransfers.api import serve_file
from .models import  Pathway101F
from .models import  Pathway102F
from .models import  Pathway2F
from .models import  Pathway3F
from .models import  Pathway4F
from .models import  Pathway5F
from .models import  Pathway6F
from .models import  Pathway7F
from .models import  PathWay1f
from .models import  Room3f
from .models import  Room4f
from .models import  Room5f
from .models import  Room6f
from .models import  Room7f
from .models import  Room101f
from .models import  Room102f
from .models import  Room1f
from .models import  Room2f
from .models import  Outline
from .models import buildingLocation
from .models import buildlist
from .models import SensorData
from .models import Install
from .models import Historydb
from .models import UploadModel


# Create your views here.

class UserViewset(viewsets.ModelViewSet):
	queryset= User.objects.all().order_by('date_joined')
	serializer_class = UserSerializer

class GroupViewset(viewsets.ModelViewSet):
	queryset= Group.objects.all()
	serializer_class = GroupSerializer

class HistorydbViewset(viewsets.ModelViewSet):
	queryset= Historydb.objects.all()
	serializer_class = HistorydbSerializer
@detail_route(methods=['POST'], permission_classes=[])

class SensorDataViewset(viewsets.ModelViewSet):
        queryset= SensorData.objects.all()
	serializer_class = SensorDataSerializer
    #def get(self, request):
	#queryset= CrowdData.objects.all()
	#serializer = CrowdDataSerializer(queryset, many=TRUE)
	#return Response(serializer.data)

   # def post(get):
	#pass

class OutlineViewset(viewsets.ModelViewSet):
	queryset= Outline.objects.all()
	serializer_class = OutlineSerializer

class buildlistViewset(viewsets.ModelViewSet):
	queryset= buildlist.objects.all()
	serializer_class =buildlistSerializer

class buildingLocationViewset(viewsets.ModelViewSet):
	queryset= buildingLocation.objects.all()
	serializer_class = buildingLocationSerializer

class Room1fViewset(viewsets.ModelViewSet):
	queryset= Room1f.objects.all()
	serializer_class = Room1fSerializer

class Room2fViewset(viewsets.ModelViewSet):
	queryset= Room2f.objects.all()
	serializer_class = Room2fSerializer

class Room3fViewset(viewsets.ModelViewSet):
	queryset= Room3f.objects.all()
	serializer_class = Room3fSerializer

class Room4fViewset(viewsets.ModelViewSet):
	queryset= Room4f.objects.all()
	serializer_class = Room4fSerializer

class Room5fViewset(viewsets.ModelViewSet):
	queryset= Room5f.objects.all()
	serializer_class = Room5fSerializer

class Room6fViewset(viewsets.ModelViewSet):
	queryset= Room6f.objects.all()
	serializer_class = Room6fSerializer

class Room7fViewset(viewsets.ModelViewSet):
	queryset= Room7f.objects.all()
	serializer_class = Room7fSerializer

class Room101fViewset(viewsets.ModelViewSet):
	queryset= Room101f.objects.all()
	serializer_class = Room101fSerializer

class Room102fViewset(viewsets.ModelViewSet):
	queryset= Room102f.objects.all()
	serializer_class = Room102fSerializer

class Pathway1fViewset(viewsets.ModelViewSet):
	queryset= PathWay1f.objects.all()
	serializer_class = Pathway1fSerializer

class Pathway101FViewset(viewsets.ModelViewSet):
	queryset= Pathway101F.objects.all()
	serializer_class = Pathway101FSerializer

class Pathway102FViewset(viewsets.ModelViewSet):
	queryset= Pathway102F.objects.all()
	serializer_class = Pathway102FSerializer

class Pathway2FViewset(viewsets.ModelViewSet):
	queryset= Pathway2F.objects.all()
	serializer_class = Pathway2FSerializer

class Pathway3FViewset(viewsets.ModelViewSet):
	queryset= Pathway3F.objects.all()
	serializer_class = Pathway3FSerializer

class Pathway4FViewset(viewsets.ModelViewSet):
	queryset= Pathway4F.objects.all()
	serializer_class = Pathway4FSerializer

class Pathway5FViewset(viewsets.ModelViewSet):
	queryset= Pathway5F.objects.all()
	serializer_class = Pathway5FSerializer

class Pathway6FViewset(viewsets.ModelViewSet):
	queryset= Pathway6F.objects.all()
	serializer_class = Pathway6FSerializer

class Pathway7FViewset(viewsets.ModelViewSet):
	queryset= Pathway7F.objects.all()
	serializer_class = Pathway7FSerializer


class InstallViewset(viewsets.ModelViewSet):
        queryset= Install.objects.all()
        serializer_class = InstallSerializer
  
@detail_route(methods=['POST'], permission_classes=[])



class UploadModelViewset(viewsets.ModelViewSet):
        queryset= UploadModel.objects.all()
        serializer_class = UploadModelSerializer

        def get_serializer_context(self):
            return {'request': self.request}

      #  def download_handler(request, pk):
       #     upload = get_object_or_404(UploadModel, pk=pk)
        #    return {'request', upload.file}




