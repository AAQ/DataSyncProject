# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.gis.db import models
import base64

# Create your models here.

class Pathway101F(models.Model):
    pid = models.IntegerField(db_column='PID', primary_key=True)  # Field name made lowercase.
    nodefrom = models.IntegerField(db_column='NodeFrom', blank=True, null=True)  # Field name made lowercase.
    nodeto = models.IntegerField(db_column='NodeTo', blank=True, null=True)  # Field name made lowercase.
    cost = models.FloatField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    geometry = models.LineStringField(db_column='geometry') 

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.pid
 
    class Meta:
       	managed = False
       	db_table = 'Pathway101F' 

class Pathway102F(models.Model):
    pid = models.IntegerField(db_column='PID', primary_key=True)  # Field name made lowercase.
    nodefrom = models.IntegerField(db_column='NodeFrom', blank=True, null=True)  # Field name made lowercase.
    nodeto = models.IntegerField(db_column='NodeTo', blank=True, null=True)  # Field name made lowercase.
    cost = models.FloatField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    geometry = models.LineStringField(db_column='geometry') 

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.pid
 
    class Meta:
       	managed = False
       	db_table = 'Pathway102F'
 
class Pathway2F(models.Model):
    pid = models.IntegerField(db_column='PID', primary_key=True)  # Field name made lowercase.
    nodefrom = models.IntegerField(db_column='NodeFrom', blank=True, null=True)  # Field name made lowercase.
    nodeto = models.IntegerField(db_column='NodeTo', blank=True, null=True)  # Field name made lowercase.
    cost = models.FloatField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    geometry = models.LineStringField(db_column='geometry') 

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.pid
 
    class Meta:
       	managed = False
       	db_table = 'Pathway2F' 

class Pathway3F(models.Model):
    pid = models.IntegerField(db_column='PID', primary_key=True)  # Field name made lowercase.
    nodefrom = models.IntegerField(db_column='NodeFrom', blank=True, null=True)  # Field name made lowercase.
    nodeto = models.IntegerField(db_column='NodeTo', blank=True, null=True)  # Field name made lowercase.
    cost = models.FloatField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    geometry = models.LineStringField(db_column='geometry') 

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.pid
 
    class Meta:
       	managed = False
       	db_table = 'Pathway3F' 

class Pathway4F(models.Model):
    pid = models.IntegerField(db_column='PID', primary_key=True)  # Field name made lowercase.
    nodefrom = models.IntegerField(db_column='NodeFrom', blank=True, null=True)  # Field name made lowercase.
    nodeto = models.IntegerField(db_column='NodeTo', blank=True, null=True)  # Field name made lowercase.
    cost = models.FloatField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    geometry = models.LineStringField(db_column='geometry') 

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.pid
 
    class Meta:
       	managed = False
       	db_table = 'Pathway4F' 

class Pathway5F(models.Model):
    pid = models.IntegerField(db_column='PID', primary_key=True)  # Field name made lowercase.
    nodefrom = models.IntegerField(db_column='NodeFrom', blank=True, null=True)  # Field name made lowercase.
    nodeto = models.IntegerField(db_column='NodeTo', blank=True, null=True)  # Field name made lowercase.
    cost = models.FloatField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    geometry = models.LineStringField(db_column='geometry') 

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.pid
 
    class Meta:
       	managed = False
       	db_table = 'Pathway5F' 

class Pathway6F(models.Model):
    pid = models.IntegerField(db_column='PID', primary_key=True)  # Field name made lowercase.
    nodefrom = models.IntegerField(db_column='NodeFrom', blank=True, null=True)  # Field name made lowercase.
    nodeto = models.IntegerField(db_column='NodeTo', blank=True, null=True)  # Field name made lowercase.
    cost = models.FloatField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    geometry = models.LineStringField(db_column='geometry') 

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.pid
 
    class Meta:
       	managed = False
       	db_table = 'Pathway6F' 

class Pathway7F(models.Model):
    pid = models.IntegerField(db_column='PID', primary_key=True)  # Field name made lowercase.
    nodefrom = models.IntegerField(db_column='NodeFrom', blank=True, null=True)  # Field name made lowercase.
    nodeto = models.IntegerField(db_column='NodeTo', blank=True, null=True)  # Field name made lowercase.
    cost = models.FloatField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    geometry = models.LineStringField(db_column='geometry') 

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.pid
 
    class Meta:
       	managed = False
       	db_table = 'Pathway7F' 

class PathWay101f_net_data(models.Model):
    Id = models.IntegerField(db_column='Id', primary_key=True) 
    NetworkData = models.BinaryField(db_column='NetworkData', null=True);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Id
 
    class Meta:
       	managed = False
       	db_table = 'PathWay101f_net_data' 

class PathWay102f_net_data(models.Model):
    Id = models.IntegerField(db_column='Id', primary_key=True) 
    NetworkData = models.BinaryField(db_column='NetworkData', null=True);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Id
 
    class Meta:
       	managed = False
       	db_table = 'PathWay102f_net_data' 

class PathWay1f_net_data(models.Model):
    Id = models.IntegerField(db_column='Id', primary_key=True) 
    NetworkData = models.BinaryField(db_column='NetworkData', null=True);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Id
 
    class Meta:
       	managed = False
       	db_table = 'PathWay1f_net_data' 

class PathWay2f_net_data(models.Model):
    Id = models.IntegerField(db_column='Id', primary_key=True) 
    NetworkData = models.BinaryField(db_column='NetworkData', null=True);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Id
 
    class Meta:
       	managed = False
       	db_table = 'PathWay2f_net_data' 

class PathWay3f_net_data(models.Model):
    Id = models.IntegerField(db_column='Id', primary_key=True) 
    NetworkData = models.BinaryField(db_column='NetworkData', null=True);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Id
 
    class Meta:
       	managed = False
       	db_table = 'PathWay3f_net_data' 

class PathWay4f_net_data(models.Model):
    Id = models.IntegerField(db_column='Id', primary_key=True) 
    NetworkData = models.BinaryField(db_column='NetworkData', null=True);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Id
 
    class Meta:
       	managed = False
       	db_table = 'PathWay4f_net_data' 

class PathWay5f_net_data(models.Model):
    Id = models.IntegerField(db_column='Id', primary_key=True) 
    NetworkData = models.BinaryField(db_column='NetworkData', null=True);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Id
 
    class Meta:
       	managed = False
       	db_table = 'PathWay5f_net_data' 

class PathWay6f_net_data(models.Model):
    Id = models.IntegerField(db_column='Id', primary_key=True) 
    NetworkData = models.BinaryField(db_column='NetworkData', null=True);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Id
 
    class Meta:
       	managed = False
       	db_table = 'PathWay6f_net_data' 

class PathWay7f_net_data(models.Model):
    Id = models.IntegerField(db_column='Id', primary_key=True) 
    NetworkData = models.BinaryField(db_column='NetworkData', null=True);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Id
 
    class Meta:
       	managed = False
       	db_table = 'PathWay7f_net_data' 

class Room3f(models.Model):
    Rid = models.IntegerField(db_column='rid', primary_key=True)
    outid = models.IntegerField(db_column='outid')
    name = models.TextField(db_column='name')
    utility = models.TextField(db_column='utility')
    floor = models.IntegerField(db_column='floor')
    location = models.PointField(db_column='location', srid=0);

    def _unicode_(self):
       	return self.Rid
 
    class Meta:
       	managed = False
       	db_table = 'Room3f' 


class Room4f(models.Model):
    Rid = models.IntegerField(db_column='rid', primary_key=True)
    outid = models.IntegerField(db_column='outid')
    name = models.TextField(db_column='name')
    utility = models.TextField(db_column='utility')
    floor = models.IntegerField(db_column='floor')
    location = models.PointField(db_column='location', srid=0);

    def _unicode_(self):
       	return self.Rid
 
    class Meta:
       	managed = False
       	db_table = 'Room4f'

class Room5f(models.Model):
    Rid = models.IntegerField(db_column='rid', primary_key=True)
    outid = models.IntegerField(db_column='outid')
    name = models.TextField(db_column='name')
    utility = models.TextField(db_column='utility')
    floor = models.IntegerField(db_column='floor')
    location = models.PointField(db_column='location', srid=0);

    def _unicode_(self):
       	return self.Rid
 
    class Meta:
       	managed = False
       	db_table = 'Room5f'

class Room6f(models.Model):
    Rid = models.IntegerField(db_column='rid', primary_key=True)
    outid = models.IntegerField(db_column='outid')
    name = models.TextField(db_column='name')
    utility = models.TextField(db_column='utility')
    floor = models.IntegerField(db_column='floor')
    location = models.PointField(db_column='location', srid=0);

    def _unicode_(self):
       	return self.Rid
 
    class Meta:
       	managed = False
       	db_table = 'Room6f'

class Room7f(models.Model):
    Rid = models.IntegerField(db_column='rid', primary_key=True)
    outid = models.IntegerField(db_column='outid')
    name = models.TextField(db_column='name')
    utility = models.TextField(db_column='utility')
    floor = models.IntegerField(db_column='floor')
    location = models.PointField(db_column='location', srid=0);

    def _unicode_(self):
       	return self.Rid
 
    class Meta:
       	managed = False
       	db_table = 'Room7f'

class Room101f(models.Model):
    Rid = models.IntegerField(db_column='rid', primary_key=True)
    outid = models.IntegerField(db_column='outid')
    name = models.TextField(db_column='name')
    utility = models.TextField(db_column='utility')
    floor = models.IntegerField(db_column='floor')
    location = models.PointField(db_column='location', srid=0);

    objects = models.GeoManager()

    def _unicode_(self):
       	return self.Rid
 
    class Meta:
       	managed = False
       	db_table = 'Room101f'     

class Room102f(models.Model):
    Rid = models.IntegerField(db_column='rid', primary_key=True)
    outid = models.IntegerField(db_column='outid')
    name = models.TextField(db_column='name')
    utility = models.TextField(db_column='utility')
    floor = models.IntegerField(db_column='floor')
    location = models.PointField(db_column='location', srid=0);

    def _unicode_(self):
       	return self.Rid
 
    class Meta:
       	managed = False
       	db_table = 'Room102f' 

class Room1f(models.Model):
    Rid = models.IntegerField(db_column='rid', primary_key=True)
    outid = models.IntegerField(db_column='outid')
    name = models.TextField(db_column='name')
    utility = models.TextField(db_column='utility')
    floor = models.IntegerField(db_column='floor')
    location = models.PointField(db_column='location', srid=0)
    NearestStaircase = models.TextField(db_column='NearestStaircase')
    buildingname = models.TextField(db_column='buildingname');

    def _unicode_(self):
       	return self.Rid
 
    class Meta:
       	managed = False
       	db_table = 'Room1f' 


class Outline(models.Model):
    outid = models.AutoField(primary_key=True)
    name = models.TextField(blank=True, null=True)
    poly = models.GeometryField(srid=0)
    shortname = models.TextField(blank=True, null=True)
    utility = models.TextField(blank=True, null=True)
    oldname = models.TextField(blank=True, null=True)
    num_floor = models.IntegerField(blank=True, null=True)
    num_basement = models.IntegerField(blank=True, null=True)
    showname = models.TextField(blank=True, null=True)
    location = models.PointField(blank=True, null=True, srid=0);
    
    def _unicode_(self):
	return self.name
    
    class Meta:
        managed = False
        db_table = 'outline' 

class Room2f(models.Model):
    Rid = models.IntegerField(db_column='rid', primary_key=True)
    outid = models.IntegerField(db_column='outid')
    name = models.TextField(db_column='name')
    utility = models.TextField(db_column='utility')
    floor = models.IntegerField(db_column='floor')
    location = models.PointField(db_column='location', srid=0)
    Geometry = models.GeometryField(srid=0);

    def _unicode_(self):
       	return self.Rid
 
    class Meta:
       	managed = False
       	db_table = 'Room2f' 

class PathWay1f(models.Model):
    PID = models.IntegerField(primary_key=True)
    NodeFrom = models.IntegerField(db_column='NodeFrom')
    NodeTo = models.IntegerField(db_column='NodeTo')
    Cost = models.FloatField(db_column='Cost', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    Geometry = models.LineStringField(db_column='geometry')    
    Indoor_more_cost = models.FloatField(blank=True, null=True)
    Indoor_pathway = models.BinaryField(null=True);

    def _unicode_(self):
       	return self.PID
 
    class Meta:
       	managed = False
       	db_table = 'PathWay1f' 



class buildingLocation(models.Model):
    outid = models.IntegerField(primary_key=True)
    showname = models.TextField(null=True)
    location = models.PointField(db_column='location', srid=0)
   
    def _unicode_(self):
       	return self.outid
 
    class Meta:
       	managed = False
       	db_table = 'buildingLocation' 


class buildlist(models.Model):
    outid = models.IntegerField(primary_key=True)
    showname = models.TextField(null=True)
    st_astext = models.TextField(null=True);
   
    def _unicode_(self):
       	return self.outid
 
    class Meta:
       	managed = False
       	db_table = 'buildlist' 


class SensorData(models.Model):
    building_id = models.TextField()
    entrance_id = models.TextField(blank=True, null=True)
    floor_level = models.TextField(blank=True, null=True)
    light_sensor = models.TextField()
    magnetic_sensor = models.TextField()
    pressure_sensor = models.TextField()
    wifi_data = models.TextField()
   
class Meta:
    
       	db_table = 'sensors_data' 
   

class Install(models.Model):
    uuid = models.TextField(blank=True, null=True)
    timeinstalled = models.TextField(blank=True, null=True)

    class Meta:
        #managed = False
        db_table = 'install'

class Historydb(models.Model):
    uuid = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    shortname = models.TextField(blank=True, null=True)
    room = models.TextField(blank=True, null=True) 
    outid = models.TextField(blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    utility = models.TextField(blank=True, null=True) 

    class Meta:
          db_table = 'historydb'


class UploadModel(models.Model):
    title = models.TextField(blank=True, null=True)
    file = models.FileField(upload_to='media', null=True)
    Upload_date = models.DateTimeField(auto_now=True)
    #path = models.FilePathField()
  #  size = models.FileSizeField()

	
    @property
    def filesize(self):
        return self.file.size

class ApiKeyToken(models.Model):
    key = models.CharField(max_length=40, primary_key=True)
    uuid = models.ForeignKey(Install)
    is_active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(ApiKeyToken, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __unicode__(self):
        return self.key
