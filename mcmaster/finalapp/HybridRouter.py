from rest_framework import routers, views, reverse, response

class HybridRouter(routers.DefaultRouter):
    def __init__(self, *args, **kwargs):
        super(HybridRouter, self).__init__(*args, **kwargs)
        self._api_view_urls = {}
    def add_api_view(self, name, url):
        self._api_view_urls[name] = url
    def remove_api_view(self, name):
        del self._api_view_urls[name]
    @property
    def api_view_urls(self):
        ret = {}
        ret.update(self._api_view_urls)
        return ret

    def get_urls(self):
        urls = super(HybridRouter, self).get_urls()
        for api_view_key in self._api_view_urls.keys():
            urls.append(self._api_view_urls[api_view_key])
        return urls

    def get_api_root_view(self):
        # Copy the following block from Default Router
        api_root_dict = {}
        list_name = self.routes[0].name
        for prefix, viewset, basename in self.registry:
            api_root_dict[prefix] = list_name.format(basename=basename)
        api_view_urls = self._api_view_urls

       

class APIRootView(views.APIView):

    """

    The default basic root view for DefaultRouter

    """

    _ignore_model_permissions = True

    exclude_from_schema = True

    api_root_dict = None



    def get(self, request, *args, **kwargs):

        # Return a plain {"name": "hyperlink"} response.

        ret = OrderedDict()

        namespace = request.resolver_match.namespace

        for key, url_name in self.api_root_dict.items():

            if namespace:

                url_name = namespace + ':' + url_name

            try:

                ret[key] = reverse(

                    url_name,

                    args=args,

                    kwargs=kwargs,

                    request=request,

                    format=kwargs.get('format', None)

                )

            except NoReverseMatch:

                # Don't bail out if eg. no list routes exist, only detail routes.

                continue



        return Response(ret)


class DefaultRouter(SimpleRouter):

    """

    The default router extends the SimpleRouter, but also adds in a default

    API root view, and adds format suffix patterns to the URLs.

    """

    include_root_view = True

    include_format_suffixes = True

    root_view_name = 'api-root'

    default_schema_renderers = None

    APIRootView = APIRootView

    APISchemaView = SchemaView

    SchemaGenerator = SchemaGenerator



    def __init__(self, *args, **kwargs):

        if 'schema_title' in kwargs:

            warnings.warn(

                "Including a schema directly via a router is now deprecated. "

                "Use `get_schema_view()` instead.",

                DeprecationWarning, stacklevel=2

            )

        if 'schema_renderers' in kwargs:

            assert 'schema_title' in kwargs, 'Missing "schema_title" argument.'

        if 'schema_url' in kwargs:

            assert 'schema_title' in kwargs, 'Missing "schema_title" argument.'

        self.schema_title = kwargs.pop('schema_title', None)

        self.schema_url = kwargs.pop('schema_url', None)

        self.schema_renderers = kwargs.pop('schema_renderers', self.default_schema_renderers)



        if 'root_renderers' in kwargs:

            self.root_renderers = kwargs.pop('root_renderers')

        else:

            self.root_renderers = list(api_settings.DEFAULT_RENDERER_CLASSES)

        super(DefaultRouter, self).__init__(*args, **kwargs)



    def get_schema_root_view(self, api_urls=None):

        """

        Return a schema root view.

        """

        schema_generator = self.SchemaGenerator(

            title=self.schema_title,

            url=self.schema_url,

            patterns=api_urls

        )



        return self.APISchemaView.as_view(

            renderer_classes=self.schema_renderers,

            schema_generator=schema_generator,

        )



    def get_api_root_view(self, api_urls=None):

        """

        Return a basic root view.

        """

        api_root_dict = OrderedDict()

        list_name = self.routes[0].name

        for prefix, viewset, basename in self.registry:

            api_root_dict[prefix] = list_name.format(basename=basename)



        return self.APIRootView.as_view(api_root_dict=api_root_dict)



    def get_urls(self):

        """

        Generate the list of URL patterns, including a default root view

        for the API, and appending `.json` style format suffixes.

        """

        urls = super(DefaultRouter, self).get_urls()



        if self.include_root_view:

            if self.schema_title:

                view = self.get_schema_root_view(api_urls=urls)

            else:

                view = self.get_api_root_view(api_urls=urls)

            root_url = url(r'^$', view, name=self.root_view_name)

            urls.append(root_url)



        if self.include_format_suffixes:

            urls = format_suffix_patterns(urls)



        return urls
