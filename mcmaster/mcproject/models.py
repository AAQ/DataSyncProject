# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.contrib.gis.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)

class BusSchedule(models.Model):
    stop_name = models.TextField()
    arrive_time = models.TextField()
    heading = models.TextField()
    utility = models.TextField()
    stop_no = models.IntegerField(blank=True, null=True)
    route_name = models.TextField()
    
    def _unicode_(self):
	return self.heading
 
    class Meta:
        managed = False
        db_table = 'bus_schedule' 

class CrowdData(models.Model):
    id = models.AutoField(primary_key=True)
    uuid = models.TextField()
    acc = models.TextField(blank=True, null=True)
    uploaded_time = models.DateTimeField(blank=True, null=True)
    gyro = models.TextField(blank=True, null=True)
    mag = models.TextField(blank=True, null=True)
    light = models.TextField(blank=True, null=True)
    wifi = models.TextField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    lon = models.FloatField(blank=True, null=True)
    floor = models.IntegerField(blank=True, null=True)
    
    def _unicode_(self):
	return self.uuid
 
    class Meta:
        managed = False
        db_table = 'crowd_data' 


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Door(models.Model):
    did = models.AutoField(primary_key=True)
    rid = models.IntegerField()
    order = models.IntegerField()
    loc = models.GeometryField()
    
    def _unicode_(self):
        return self.rid
 
    class Meta:
        managed = False
        db_table = 'door' 

class MapError(models.Model):
    building = models.TextField()
    floor = models.IntegerField()
    description = models.TextField()
    room = models.TextField(blank=True, null=True)
    uploaded_time = models.DateTimeField(blank=True, null=True)
    
    def __str__(self):
	return self.building
 
    class Meta:
        managed = False
        db_table = 'map_error' 

class Outline(models.Model):
    outid = models.AutoField(primary_key=True)
    name = models.TextField(blank=True, null=True)
    poly = models.GeometryField(srid=0)
    shortname = models.TextField(blank=True, null=True)
    utility = models.TextField(blank=True, null=True)
    oldname = models.TextField(blank=True, null=True)
    num_floor = models.IntegerField(blank=True, null=True)
    num_basement = models.IntegerField(blank=True, null=True)
    showname = models.TextField(blank=True, null=True)
    
    def _unicode_(self):
	return self.name
    
    class Meta:
        managed = False
        db_table = 'outline' 

class OutlineNamePosition(models.Model):
    npid = models.IntegerField(blank=True, null=True)
    outid = models.IntegerField(blank=True, null=True)
    showname = models.CharField(max_length=100, blank=True, null=True)
    point = models.PointField(srid=0, blank=True, null=True)
   
    def _unicode_(self):
	return self.npid
 
    class Meta:
        managed = False
        db_table = 'outline_name_position' 

class Pathway(models.Model):
    pid = models.AutoField(primary_key=True)
    line = models.GeometryField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    utility = models.TextField(blank=True, null=True)
    outid = models.IntegerField()
    maptype = models.TextField(blank=True, null=True)
    floor = models.IntegerField()
    source = models.IntegerField(blank=True, null=True)
    target = models.IntegerField(blank=True, null=True)
    cost = models.FloatField(blank=True, null=True)
    reverse_cost = models.FloatField(blank=True, null=True)
    old_id = models.IntegerField(blank=True, null=True)
    complex_line = models.GeometryField(srid=0, blank=True, null=True)
    ifprocessed = models.NullBooleanField()
    highway = models.TextField(blank=True, null=True)
    aeroway = models.TextField(blank=True, null=True)
    
    def _unicode_(self):
	return self.name
 
    class Meta:
        managed = False
        db_table = 'pathway' 

class Pathway0FVerticesPgr(models.Model):
    id = models.BigAutoField(primary_key=True)
    cnt = models.IntegerField(blank=True, null=True)
    chk = models.IntegerField(blank=True, null=True)
    ein = models.IntegerField(blank=True, null=True)
    eout = models.IntegerField(blank=True, null=True)
    the_geom = models.PointField(blank=True, null=True)
    
    def _unicode_(self):
	return self.cnt
 
    class Meta:
        managed = False
        db_table = 'pathway0f_vertices_pgr' 

class User(models.Model):
    uuid = models.TextField(blank=True, null=True)
    timeinstalled = models.DateTimeField(blank=True, null=True)

    def _unicode_(self):
 	return self.uuid 

    class Meta:
        managed = False
        db_table = 'user'

