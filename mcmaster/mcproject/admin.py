# -*- coding: utf-8 -*-
from __future__ import unicode_literals 
from django.contrib import admin 
from django.contrib import admin 
from leaflet.admin import LeafletGeoAdmin 
from django.contrib.gis import admin
from models import *

# Register your models here.

class DoorAdmin(LeafletGeoAdmin):
	search_fields=('did','rid', 'loc')
		
class BusScheduleAdmin(LeafletGeoAdmin):
	search_fields=('stop_name','stop_no','route_name')

class CrowdDataAdmin(LeafletGeoAdmin):
	search_fields=('id','uuid','lat','lon')

class OutlineAdmin(LeafletGeoAdmin):
	search_fields=('outid','name','oudname')

class OutlineNamePositionAdmin(LeafletGeoAdmin):
	search_fields=('outid','name','poly')

class PathwayAdmin(LeafletGeoAdmin):
	search_fields=('pid','cost','utility')

class UserAdmin(LeafletGeoAdmin):
	search_fields=('uuid','timeinstalled',)

admin.site.register(Door, DoorAdmin)
admin.site.register(BusSchedule, BusScheduleAdmin)
admin.site.register(CrowdData, CrowdDataAdmin)
admin.site.register(Outline, OutlineAdmin)
admin.site.register(OutlineNamePosition, OutlineNamePositionAdmin)
admin.site.register(Pathway, PathwayAdmin)
admin.site.register(User, UserAdmin)
